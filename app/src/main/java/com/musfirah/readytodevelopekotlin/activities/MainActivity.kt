package com.musfirah.readytodevelopekotlin.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.musfirah.readytodevelopekotlin.R
import com.musfirah.readytodevelopekotlin.databinding.ActivityDrawerHomeBinding
import com.musfirah.readytodevelopekotlin.fragments.*
import com.musfirah.readytodevelopekotlin.utils.CommonMethods
import com.musfirah.readytodevelopekotlin.utils.CommonObjects

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDrawerHomeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityDrawerHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        CommonObjects.context=this

        initViews()
        openScreen(HomeQuranFragment.newInstance())
    }

    private fun openScreen(fragment:BaseFragment) {
        CommonMethods.callFragment(fragment, binding.activityMain.flFragmentContainer.id, R.anim.fade_in, R.anim.fade_out, this, false)
    }

    private fun initViews(){
        inItSideMenu()
        initTopBar()
        inItBottomNavigation()
    }

    private fun initTopBar() {
        binding.activityMain.topBar.ibMenu.setOnClickListener(
            View.OnClickListener { binding.drawerLayout.openDrawer(binding.nvLeft) })
    }

    private fun inItSideMenu() {
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
//        termsAndConditionSettings = CommonMethods.getTermsAndConditionSettings(MainActivity.instance())
//        if (termsAndConditionSettings == null) {
//            loadAndSaveTermsAndConditionData()
//        }
//        llContactUs.setOnClickListener(View.OnClickListener {
//            drawer.closeDrawer(nvLeft)
//            CommonMethods.showToast("contact us", Toast.LENGTH_LONG)
//        })
//        llAboutUs.setOnClickListener(View.OnClickListener {
//            if (termsAndConditionSettings != null) {
//                drawer.closeDrawer(nvLeft)
//                CommonMethods.callFragment(
//                    TermsConditionPrivacyPolicyFragment.newInstance(
//                        "About Us",
//                        termsAndConditionSettings.getAbout()
//                    ),
//                    R.id.flFragmentOverlay,
//                    R.anim.fade_in,
//                    R.anim.fade_out,
//                    this@MainActivity,
//                    true
//                )
//            }
//        })
//        llPrivacyPolicy.setOnClickListener(View.OnClickListener {
//            if (termsAndConditionSettings != null) {
//                drawer.closeDrawer(nvLeft)
//                CommonMethods.callFragment(
//                    TermsConditionPrivacyPolicyFragment.newInstance(
//                        "Privacy Policy",
//                        termsAndConditionSettings.getPolicy()
//                    ),
//                    R.id.flFragmentOverlay,
//                    R.anim.fade_in,
//                    R.anim.fade_out,
//                    this@MainActivity,
//                    true
//                )
//            }
//        })
//        llTermsAndCondition.setOnClickListener(View.OnClickListener {
//            if (termsAndConditionSettings != null) {
//                drawer.closeDrawer(nvLeft)
//                CommonMethods.callFragment(
//                    TermsConditionPrivacyPolicyFragment.newInstance(
//                        "Terms & Condition",
//                        termsAndConditionSettings.getTerms()
//                    ),
//                    R.id.flFragmentOverlay,
//                    R.anim.fade_in,
//                    R.anim.fade_out,
//                    this@MainActivity,
//                    true
//                )
//            }
//        })
    }

    private fun inItBottomNavigation() {
        binding.activityMain.bottomNavigatinView.menu.findItem(R.id.navigation_home).setChecked(true)
        binding.activityMain.bottomNavigatinView.setOnItemSelectedListener {
            when(it.itemId){
                R.id.navigation_home->{
                    if (binding.activityMain.bottomNavigatinView.getSelectedItemId() != R.id.navigation_home) {
                        //TODO open screen relative
                        openScreen(HomeQuranFragment.newInstance())
                    }

                    return@setOnItemSelectedListener true
                }
                R.id.navigation_namaz->{
                    if (binding.activityMain.bottomNavigatinView.getSelectedItemId() != R.id.navigation_namaz) {
                        //TODO open screen relative
                        openScreen(HomeNamazFragment.newInstance())
                    }

                return@setOnItemSelectedListener true
                }
                R.id.navigation_bookmark->{
                    if (binding.activityMain.bottomNavigatinView.getSelectedItemId() != R.id.navigation_bookmark) {
                        //TODO open screen relative
                        openScreen(HomeBookmarkFragment.newInstance())
                    }

                return@setOnItemSelectedListener true
                }
            }
            return@setOnItemSelectedListener false
        }
    }


}
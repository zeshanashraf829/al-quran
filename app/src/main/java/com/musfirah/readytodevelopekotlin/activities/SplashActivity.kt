package com.musfirah.readytodevelopekotlin.activities

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.musfirah.readytodevelopekotlin.databinding.ActivitySplashBinding
import com.musfirah.readytodevelopekotlin.utils.CommonMethods
import com.musfirah.readytodevelopekotlin.utils.CommonObjects

class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        CommonObjects.context=this

        Handler(Looper.getMainLooper()).postDelayed(Runnable { goToHomeScreen() },3000)
    }

    private fun goToHomeScreen(){
        CommonMethods.callActivity(this,this,MainActivity(),true,true)
    }


}
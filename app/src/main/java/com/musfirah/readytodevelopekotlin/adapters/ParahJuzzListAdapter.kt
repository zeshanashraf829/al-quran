package com.musfirah.readytodevelopekotlin.adapters

import android.content.Context
import android.hardware.biometrics.BiometricManager
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.musfirah.readytodevelopekotlin.R
import com.musfirah.readytodevelopekotlin.business.models.quran.juzz.Juzz
import com.musfirah.readytodevelopekotlin.business.models.quran.surah.Surah
import com.musfirah.readytodevelopekotlin.utils.CommonMethods
import java.lang.Exception
import java.util.ArrayList

/**
 * Created by Rana Zeshan on 27-Jun-19.
 */
class ParahJuzzListAdapter(private val listItems: ArrayList<Juzz>) :
    RecyclerView.Adapter<ParahJuzzListAdapter.ViewHolder?>() {
    private var context: Context? = null


    inner class ViewHolder(val mView: View): RecyclerView.ViewHolder(mView){
        fun bindItems(juzz: Juzz) {
            itemView.findViewById<TextView>(R.id.tvNumber).text="${juzz.paraID}"
            itemView.findViewById<TextView>(R.id.tvJuzzNameEnglish).text="${juzz.transliteration}"
            itemView.findViewById<TextView>(R.id.tvJuzzNameArabic).text="${juzz.name}"
            itemView.findViewById<TextView>(R.id.tvJuzzDescription).text="${juzz.juzzNumber}"
        }
    }


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (context == null) {
            context = parent.getContext()
        }

        return ViewHolder(CommonMethods.createView(context!!, R.layout.list_item_juzz, parent)!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        final String title = listItems.get(position);

        holder.mView.setOnClickListener { CommonMethods.showToast(context!!,"Test",Toast.LENGTH_LONG) }
        holder.bindItems(listItems.get(position))
//        Log.e("Title a",title);
        try {
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        return listItems?.size
    }
}
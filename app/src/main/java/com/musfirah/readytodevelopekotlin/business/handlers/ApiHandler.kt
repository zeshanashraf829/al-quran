package com.musfirah.readytodevelopekotlin.business.handlers

import android.util.Log
import androidx.fragment.app.FragmentActivity
import com.androidnetworking.common.Priority
import com.google.gson.Gson
import com.musfirah.readytodevelopekotlin.business.interfaces.OnQuizLoadListener
import com.musfirah.readytodevelopekotlin.business.interfaces.OnServerResultNotifier
import com.musfirah.readytodevelopekotlin.business.interfaces.QuranSectionListener
import com.musfirah.readytodevelopekotlin.business.models.quran.ayaat.QuranAyaatResponse
import com.musfirah.readytodevelopekotlin.business.models.quran.surah.QuranSurahResponse
import com.musfirah.readytodevelopekotlin.utils.CommonMethods
import com.musfirah.readytodevelopekotlin.utils.CommonObjects

class ApiHandler {
    companion object{
        fun callGetApi(amount:Int,onQuizLoadListener:OnQuizLoadListener){
            var url:String="https://opentdb.com/api.php?amount=${amount}"
            CallForServer(url, object : OnServerResultNotifier {
                override fun onServerResultNotifier(isError: Boolean, response: String?) {

//                CommonMethods.hideProgressDialog();
                    if (!isError) {
                        onQuizLoadListener.onSuccess(response)
                    } else {
                        onQuizLoadListener.onError(response)
                    }
                }
            }).callForServerGet("quizApi",Priority.LOW)
        }

        fun ReadQuranJsonFromFile(
            fileName: String,
            quranSectionListener: QuranSectionListener
        ) {
            CommonMethods.showProgressDialog(CommonObjects.context as FragmentActivity)
            CallForServer(
                fileName,
                object : OnServerResultNotifier {
                    override fun onServerResultNotifier(isError: Boolean, response: String?) {
                        Log.e("quranResponse", "" + response)
                        CommonMethods.hideProgressDialog()
                        if (!isError) {
                            try {
                                if (fileName.equals("ayaat.json", ignoreCase = true)) {
                                    val quranAyaatResponse: QuranAyaatResponse = Gson().fromJson(response, QuranAyaatResponse::class.java)
                                    quranSectionListener.onSuccess(quranAyaatResponse)
                                } else if (fileName.equals("surah.json", ignoreCase = true)) {
                                    val quranSurahResponse: QuranSurahResponse = Gson().fromJson(
                                        response,
                                        QuranSurahResponse::class.java
                                    )
                                    quranSectionListener.onSuccess(quranSurahResponse)
                                }else{
                                    quranSectionListener.onSuccess(response)
                                }
                            } catch (e: Exception) {
                                quranSectionListener.onError(e.message)
                            }
                        } else {
                            quranSectionListener.onError(response)
                        }
                    }

                }).callForLocalArray()
        }

    }
}
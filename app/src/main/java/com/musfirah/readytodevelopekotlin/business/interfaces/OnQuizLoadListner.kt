package com.musfirah.readytodevelopekotlin.business.interfaces

interface OnQuizLoadListener {
    fun onSuccess(response: String?)
    fun onError(error: String?)
}
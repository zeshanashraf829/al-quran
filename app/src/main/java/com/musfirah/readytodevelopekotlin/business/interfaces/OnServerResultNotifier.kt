package com.musfirah.readytodevelopekotlin.business.interfaces

interface OnServerResultNotifier {
    fun onServerResultNotifier(isError: Boolean, response: String?)

}
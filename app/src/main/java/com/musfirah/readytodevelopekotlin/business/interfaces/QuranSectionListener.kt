package com.musfirah.readytodevelopekotlin.business.interfaces

import com.musfirah.readytodevelopekotlin.business.models.quran.ayaat.QuranAyaatResponse
import com.musfirah.readytodevelopekotlin.business.models.quran.surah.QuranSurahResponse

interface QuranSectionListener {

    fun onSuccess(quranAyaatResponse: QuranAyaatResponse?)
    fun onSuccess(quranSurahResponse: QuranSurahResponse?)
    fun onSuccess(response: String?)
    fun onError(error: String?)
}
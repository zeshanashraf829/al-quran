package com.musfirah.readytodevelopekotlin.business.models.quran.ayaat

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose
import com.musfirah.readytodevelopekotlin.business.models.quran.ayaat.Ayaat

class Ayaat {
    @SerializedName("AyaID")
    @Expose
    var ayaID: Int? = null

    @SerializedName("SuraID")
    @Expose
    var suraID: Int? = null

    @SerializedName("AyaNo")
    @Expose
    var ayaNo: Int? = null

    @SerializedName("Arabic Text")
    @Expose
    var arabicText: String? = null

    @SerializedName("RakuID")
    @Expose
    var rakuID: Int? = null

    @SerializedName("PRakuID")
    @Expose
    var pRakuID: Int? = null

    @SerializedName("ParaID")
    @Expose
    var paraID: Int? = null
}
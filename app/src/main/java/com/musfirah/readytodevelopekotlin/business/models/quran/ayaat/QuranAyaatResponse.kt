package com.musfirah.readytodevelopekotlin.business.models.quran.ayaat

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose
import com.musfirah.readytodevelopekotlin.business.models.quran.ayaat.Ayaat

class QuranAyaatResponse {
    @SerializedName("ayaat")
    @Expose
    var ayaat: List<Ayaat>? = null
}
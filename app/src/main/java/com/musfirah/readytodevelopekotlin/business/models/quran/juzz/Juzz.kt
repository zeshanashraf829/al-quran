package com.musfirah.readytodevelopekotlin.business.models.quran.juzz

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose
import com.musfirah.readytodevelopekotlin.business.models.quran.juzz.Juzz

class Juzz {
    @SerializedName("ParaID")
    @Expose
    var paraID: Int? = null

    @SerializedName("JuzzNumber")
    @Expose
    var juzzNumber: String? = null

    @SerializedName("Name")
    @Expose
    var name: String? = null

    @SerializedName("Transliteration")
    @Expose
    var transliteration: String? = null

    @SerializedName("SurahVerse")
    @Expose
    var surahVerse: String? = null

    @SerializedName("Save")
    @Expose
    var save: String? = null
}
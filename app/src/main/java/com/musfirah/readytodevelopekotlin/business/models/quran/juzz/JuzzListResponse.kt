package com.musfirah.readytodevelopekotlin.business.models.quran.juzz

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose
import com.musfirah.readytodevelopekotlin.business.models.quran.juzz.Juzz

class JuzzListResponse {
    @SerializedName("juzz")
    @Expose
    var juzz: List<Juzz>? = null
}
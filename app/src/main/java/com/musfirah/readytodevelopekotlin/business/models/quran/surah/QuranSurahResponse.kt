package com.musfirah.readytodevelopekotlin.business.models.quran.surah

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose
import com.musfirah.readytodevelopekotlin.business.models.quran.surah.Surah

class QuranSurahResponse {
    @SerializedName("surah")
    @Expose
    var surah: List<Surah>? = null
}
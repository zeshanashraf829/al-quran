package com.musfirah.readytodevelopekotlin.business.models.quran.surah

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose
import com.musfirah.readytodevelopekotlin.business.models.quran.surah.Surah

class Surah {
    @SerializedName("SurahID")
    @Expose
    var surahID: Int? = null

    @SerializedName("SurahNameE")
    @Expose
    var surahNameE: String? = null

    @SerializedName("Nazool")
    @Expose
    var nazool: String? = null

    @SerializedName("SurahNameU")
    @Expose
    var surahNameU: String? = null
}
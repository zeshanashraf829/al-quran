package com.musfirah.readytodevelopekotlin.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.musfirah.readytodevelopekotlin.business.handlers.ApiHandler
import com.musfirah.readytodevelopekotlin.business.interfaces.OnQuizLoadListener
import com.musfirah.readytodevelopekotlin.databinding.EmptyFragmentBinding
import com.musfirah.readytodevelopekotlin.databinding.HomeBookmarkFragmentBinding
import com.musfirah.readytodevelopekotlin.databinding.HomeNamazFragmentBinding
import com.musfirah.readytodevelopekotlin.databinding.HomeQuranFragmentBinding
import com.musfirah.readytodevelopekotlin.utils.CommonMethods

class HomeBookmarkFragment : BaseFragment() {
    private lateinit var binding: HomeBookmarkFragmentBinding

    companion object {
        fun newInstance(): HomeBookmarkFragment {
            return HomeBookmarkFragment()
        }
    }

    override fun initView() {
        CommonMethods.setupUI(binding.root, mActivity)
    }

    override fun loadData() {
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = HomeBookmarkFragmentBinding.inflate(layoutInflater)
        setBinding(binding)
        return binding.root
    }

    override val fragmentTag: String
        get() = HomeBookmarkFragment::class.java.simpleName

}
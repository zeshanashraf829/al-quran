package com.musfirah.readytodevelopekotlin.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.musfirah.readytodevelopekotlin.databinding.HomeNamazFragmentBinding
import com.musfirah.readytodevelopekotlin.utils.CommonMethods


class HomeNamazFragment : BaseFragment() {
    private lateinit var binding: HomeNamazFragmentBinding

    companion object {
        fun newInstance(): HomeNamazFragment {
            return HomeNamazFragment()
        }
    }

    override fun initView() {
        binding.tvScreen.setOnClickListener {  }
        CommonMethods.setupUI(binding.root, mActivity)
    }

    override fun loadData() {
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = HomeNamazFragmentBinding.inflate(layoutInflater)
        setBinding(binding)
        return binding.root
    }

    override val fragmentTag: String
        get() = HomeNamazFragment::class.java.simpleName

}
package com.musfirah.readytodevelopekotlin.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.musfirah.readytodevelopekotlin.adapters.ViewPageAdapter
import com.musfirah.readytodevelopekotlin.databinding.HomeQuranFragmentBinding
import com.musfirah.readytodevelopekotlin.fragments.quranScreenPages.ByJuzzParahListFragment
import com.musfirah.readytodevelopekotlin.fragments.quranScreenPages.FavoriteListFragment
import com.musfirah.readytodevelopekotlin.utils.CommonMethods

class HomeQuranFragment : BaseFragment() {
    private lateinit var binding: HomeQuranFragmentBinding
    private var viewPageAdapter: ViewPageAdapter? = null

    companion object {
        fun newInstance(): HomeQuranFragment {
            return HomeQuranFragment()
        }
    }

    override fun initView() {
        initTabsPagerView()
        CommonMethods.setupUI(binding.root, mActivity)
    }

    override fun loadData() {
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = HomeQuranFragmentBinding.inflate(layoutInflater)
        setBinding(binding)
        return binding.root
    }

    override val fragmentTag: String
        get() = HomeQuranFragment::class.java.simpleName

    fun initTabsPagerView() {
        viewPageAdapter = ViewPageAdapter(childFragmentManager, PagerAdapter.POSITION_NONE)
        viewPageAdapter?.addFragment(FavoriteListFragment.newInstance(), "By Surah")
        viewPageAdapter?.addFragment(ByJuzzParahListFragment.newInstance(), "By Parah")
        viewPageAdapter?.addFragment(FavoriteListFragment.newInstance(), "Favorites")
        binding.vpQuranicList.setOffscreenPageLimit(1)
        binding.vpQuranicList.setAdapter(viewPageAdapter)
        binding.tlTabs.setupWithViewPager(binding.vpQuranicList)
    }

}
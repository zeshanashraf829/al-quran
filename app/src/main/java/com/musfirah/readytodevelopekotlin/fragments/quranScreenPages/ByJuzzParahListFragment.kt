package com.musfirah.readytodevelopekotlin.fragments.quranScreenPages

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.musfirah.readytodevelopekotlin.adapters.FavoriteListAdapter
import com.musfirah.readytodevelopekotlin.adapters.ParahJuzzListAdapter
import com.musfirah.readytodevelopekotlin.business.handlers.ApiHandler
import com.musfirah.readytodevelopekotlin.business.interfaces.QuranSectionListener
import com.musfirah.readytodevelopekotlin.business.models.quran.ayaat.QuranAyaatResponse
import com.musfirah.readytodevelopekotlin.business.models.quran.juzz.Juzz
import com.musfirah.readytodevelopekotlin.business.models.quran.juzz.JuzzListResponse
import com.musfirah.readytodevelopekotlin.business.models.quran.surah.QuranSurahResponse
import com.musfirah.readytodevelopekotlin.business.models.quran.surah.Surah
import com.musfirah.readytodevelopekotlin.databinding.SurahListFragmentBinding
import com.musfirah.readytodevelopekotlin.fragments.BaseFragment
import com.musfirah.readytodevelopekotlin.utils.CommonMethods
import com.musfirah.readytodevelopekotlin.utils.CommonObjects
import java.lang.Exception

/**
 * Created by Rana Zeshan on 27-Jun-19.
 */
class ByJuzzParahListFragment : BaseFragment() {
    private val listSurah: ArrayList<Juzz> = ArrayList()
    private var binding: SurahListFragmentBinding? = null
    override fun initView() {
        binding?.rvListItems?.setLayoutManager(
            LinearLayoutManager(
                activity,
                LinearLayoutManager.VERTICAL,
                false
            )
        )

        binding?.rvListItems?.setAdapter(ParahJuzzListAdapter(listSurah))
        showHideNoItemsMessage(false)
        CommonMethods.setupUI(binding?.root!!, mActivity)
    }

    override fun loadData() {
        if(listSurah.isNullOrEmpty()){
            showHideNoItemsMessage(true)
            getQuranJuzzDetails()
        }else{
            showHideNoItemsMessage(false)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding= SurahListFragmentBinding.inflate(layoutInflater)
        return binding?.root
    }

    override val fragmentTag: String
        get() = ByJuzzParahListFragment::class.java.simpleName

    fun showHideNoItemsMessage(isShow: Boolean) {
        if (isShow) {
            binding?.llNoItems!!.visibility = View.VISIBLE
            binding?.rvListItems!!.visibility = View.GONE
        } else {
            binding?.llNoItems!!.visibility = View.GONE
            binding?.rvListItems!!.visibility = View.VISIBLE
        }
    }

    companion object {
        fun newInstance(): ByJuzzParahListFragment {
            return ByJuzzParahListFragment()
        }
    }

    fun getQuranJuzzDetails() {
        CommonMethods.showProgressDialog(CommonObjects.context as FragmentActivity)
        ApiHandler.ReadQuranJsonFromFile("juzz_list.json",object : QuranSectionListener {
            override fun onSuccess(quranAyaatResponse: QuranAyaatResponse?) {
                showHideNoItemsMessage(false)
                CommonMethods.hideProgressDialog()

            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onSuccess(quranSurahResponse: QuranSurahResponse?) {
                showHideNoItemsMessage(false)
                CommonMethods.hideProgressDialog()

            }


            override fun onSuccess(response: String?) {
                showHideNoItemsMessage(false)
                CommonMethods.hideProgressDialog()
                try{
                    val juzzListResponse: JuzzListResponse = Gson().fromJson(response, JuzzListResponse::class.java)
                    juzzListResponse?.juzz?.let {
                        if(!it.isNullOrEmpty()){
                            listSurah.clear()
                            listSurah.addAll(it)
                            binding?.rvListItems?.adapter?.notifyDataSetChanged()
                        }else{
                            showHideNoItemsMessage(true)
                            CommonMethods.hideProgressDialog()
                        }
                    } ?: run{
                        showHideNoItemsMessage(true)
                        CommonMethods.hideProgressDialog()
                    }
                }catch (e:Exception){
                    e.printStackTrace()
                    showHideNoItemsMessage(true)
                    CommonMethods.hideProgressDialog()
                    CommonMethods.showMessage(CommonObjects.context, "Juzz: ${e.message}")
                }
            }

            override fun onError(error: String?) {
                showHideNoItemsMessage(true)
                CommonMethods.hideProgressDialog()
                CommonMethods.showMessage(CommonObjects.context, "Juzz: $error")
            }
        })
    }
}
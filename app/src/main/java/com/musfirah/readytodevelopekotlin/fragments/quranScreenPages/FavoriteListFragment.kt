package com.musfirah.readytodevelopekotlin.fragments.quranScreenPages

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.musfirah.readytodevelopekotlin.activities.MainActivity
import com.musfirah.readytodevelopekotlin.adapters.FavoriteListAdapter
import com.musfirah.readytodevelopekotlin.business.handlers.ApiHandler
import com.musfirah.readytodevelopekotlin.business.interfaces.QuranSectionListener
import com.musfirah.readytodevelopekotlin.business.models.quran.ayaat.QuranAyaatResponse
import com.musfirah.readytodevelopekotlin.business.models.quran.surah.QuranSurahResponse
import com.musfirah.readytodevelopekotlin.business.models.quran.surah.Surah
import com.musfirah.readytodevelopekotlin.databinding.SurahListFragmentBinding
import com.musfirah.readytodevelopekotlin.fragments.BaseFragment
import com.musfirah.readytodevelopekotlin.utils.CommonMethods
import com.musfirah.readytodevelopekotlin.utils.CommonObjects

/**
 * Created by Rana Zeshan on 27-Jun-19.
 */
class FavoriteListFragment : BaseFragment() {
    private val listSurah: ArrayList<Surah> = ArrayList()
    private var binding: SurahListFragmentBinding? = null
    override fun initView() {
        binding?.rvListItems?.setLayoutManager(
            LinearLayoutManager(
                activity,
                LinearLayoutManager.VERTICAL,
                false
            )
        )

        binding?.rvListItems?.setAdapter(FavoriteListAdapter(listSurah))
        showHideNoItemsMessage(false)
        CommonMethods.setupUI(binding?.root!!, mActivity)
    }

    override fun loadData() {
        if(listSurah.isNullOrEmpty()){
            showHideNoItemsMessage(true)
            getQuranSurahDetailsDetails()
        }else{
            showHideNoItemsMessage(false)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding= SurahListFragmentBinding.inflate(layoutInflater)
        return binding?.root
    }

    override val fragmentTag: String
        get() = FavoriteListFragment::class.java.simpleName

    fun showHideNoItemsMessage(isShow: Boolean) {
        if (isShow) {
            binding?.llNoItems!!.visibility = View.VISIBLE
            binding?.rvListItems!!.visibility = View.GONE
        } else {
            binding?.llNoItems!!.visibility = View.GONE
            binding?.rvListItems!!.visibility = View.VISIBLE
        }
    }

    companion object {
        fun newInstance(): FavoriteListFragment {
            return FavoriteListFragment()
        }
    }

    fun getQuranSurahDetailsDetails() {
        CommonMethods.showProgressDialog(CommonObjects.context as FragmentActivity)
        ApiHandler.ReadQuranJsonFromFile("surah.json",object : QuranSectionListener {
            override fun onSuccess(quranAyaatResponse: QuranAyaatResponse?) {
                showHideNoItemsMessage(false)

            }
            override fun onSuccess(response: String?) {
                showHideNoItemsMessage(false)

            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onSuccess(quranSurahResponse: QuranSurahResponse?) {
                showHideNoItemsMessage(false)
                CommonMethods.hideProgressDialog()

                quranSurahResponse?.surah?.let {
                    if (it.isNotEmpty()) {
                        listSurah.clear()
                        listSurah.addAll(it)
                        binding?.rvListItems?.adapter?.notifyDataSetChanged()
                    }

                }


            }

            override fun onError(error: String?) {
                showHideNoItemsMessage(true)
                CommonMethods.hideProgressDialog()
                CommonMethods.showMessage(CommonObjects.context, "Surah: $error")
            }
        })
    }
}
package com.musfirah.readytodevelopekotlin.preferences

import android.app.Activity
import android.content.Context

import android.content.SharedPreferences
import com.musfirah.readytodevelopekotlin.R


class PreferencesUtils(mContext: Context) {

    private var mInstance: PreferencesUtils? = null
    private var preferences: SharedPreferences? = null
    private var editor: SharedPreferences.Editor? = null

    private var PREF_NAME: String? = null

    init {
        PREF_NAME = mContext.getString(R.string.app_name)
        preferences = mContext.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE)
        editor = preferences!!.edit()
    }


    @Synchronized
    fun getInstance(mContext: Context): PreferencesUtils? {
        if (mInstance == null) {
            mInstance = PreferencesUtils(mContext)
        }
        return mInstance
    }

    fun putString(key: String?, value: String?) {
        editor!!.putString(key, value).commit()
    }

    fun getString(key: String?, defValue: String?): String? {
        return preferences!!.getString(key, defValue)
    }

    fun putInt(key: String?, value: Int) {
        editor!!.putInt(key, value).commit()
    }

    fun getInt(key: String?, defValue: Int): Int {
        return preferences!!.getInt(key, defValue)
    }

    fun putFloat(key: String?, value: Float) {
        editor!!.putFloat(key, value).commit()
    }

    fun getFloat(key: String?, defValue: Float): Float {
        return preferences!!.getFloat(key, defValue)
    }

    fun putBoolean(key: String?, value: Boolean) {
        editor!!.putBoolean(key, value).commit()
    }

    fun getBoolean(key: String?, defValue: Boolean): Boolean {
        return preferences!!.getBoolean(key, defValue)
    }

    fun putLong(key: String?, value: Long?) {
        editor!!.putLong(key, value!!).commit()
    }

    fun getLong(key: String?): Long {
        return preferences!!.getLong(key, 0L)
    }

    /**
     * Here clear the shared preferences and restore the user first time install key value as false,
     * because user is launched this app already
     */
    fun clearPreferences() {
        if (editor != null) {
            editor!!.clear().commit()
        }
    }
}
